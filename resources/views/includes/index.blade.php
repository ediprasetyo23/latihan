@extends('layouts/default')

@section('content')

<div class="banner-content text-center">
<h1 class="text-uppercase text-white"><span>Biodata</span></h1>
<br>
<button type="button" class="btn btn-info" id="addbutton"> Tambah Biodata </button>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body" id="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<table class="table table-striped" style="border:3px solid black;color:black;background:white">

	<tr style="border:2px solid black">
		<th>Nama</th>
		<th>Gender</th>
		<th>TTL</th>
		<th>Alamat</th>
		<th></th>
		<th></th>
	</tr>

	@foreach($biodata as $bio)
	
	<tr>
		<td>{{ $bio->nama }}</td>
		<td>{{ $bio->gender }}</td>
		<td>{{ $bio->ttl }}</td>
		<td>{{ $bio->alamat }}</td>
		<td>
			<button class="btn btn-warning" id="editbutton" onclick="confirm_dit(this.value)" value="{{ $bio->id }}">Edit</button>
			
		</td>
		<td>
			<button class="btn btn-danger" value="{{ $bio->id }}" onclick="confirm_del(this.value)">Delete</button>
		</td>
	</tr>
	
	@endforeach

</table>
</div>
<script type="text/javascript">
	$('#addbutton').click(function(){
		$('#myModal').modal();
		$.ajax({
			url:"/biodata/add",
			type:"get",
			dataType:"html",
			success:function(data){
				document.getElementById('myModalLabel').innerHTML="Tambah Data";

				document.getElementById('modal-body').innerHTML=data;
			}
		})
	});
	function confirm_dit(id){
		/*console.log(id);*/
		$('#myModal').modal();
		$.ajax({
			url:"/biodata/edit/"+id,
			type:"get",
			dataType:"html",
			success:function(data){
				document.getElementById('myModalLabel').innerHTML="Tambah Data";

				document.getElementById('modal-body').innerHTML=data;
			}
		})
	}
	function confirm_del(id){
		var conf=confirm("Delete confirm");
		if(conf){
			$.ajax({
			url:"/biodata/delete/"+id,
			type:"get",
		})
			location.replace('index');
		}
	}

	/*$('#editbutton').click(function(){
		$id = $('#editbutton').val();
		console.log($id);
		$('#myModal').modal();
		$.ajax({
			url:"/biodata/edit/"+$id,
			type:"get",
			dataType:"html",
			success:function(data){
				document.getElementById('myModalLabel').innerHTML="Tambah Data";

				document.getElementById('modal-body').innerHTML=data;
			}
		})
	});*/	
</script>

@stop