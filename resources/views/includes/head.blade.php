		<link href="https://fonts.googleapis.com/css?family=Poppins:100,300,500,600" rel="stylesheet">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="{{ asset('asset/css/linearicons.css') }}">
		<link rel="stylesheet" href="{{ asset('asset/css/font-awesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('asset/css/nice-select.css') }}">
		<link rel="stylesheet" href="{{ asset('asset/css/magnific-popup.css') }}">
		<link rel="stylesheet" href="{{ asset('asset/css/bootstrap.css') }}">
		<link rel="stylesheet" href="{{ asset('asset/css/main.css') }}">

		<script src="{{ asset('asset/js/vendor/jquery-2.2.4.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="{{ asset('asset/js/vendor/bootstrap.min.js') }}"></script>
		<script src="{{ asset('asset/js/jquery.ajaxchimp.min.js') }}"></script>
		<script src="{{ asset('asset/js/jquery.nice-select.min.js') }}"></script>
		<script src="{{ asset('asset/js/jquery.magnific-popup.min.js') }}"></script>
		<script src="{{ asset('asset/js/waypoints.min.js') }}"></script>
		<script src="{{ asset('asset/js/jquery.counterup.min.js') }}"></script>
		<script src="{{ asset('asset/js/main.js') }}"></script>