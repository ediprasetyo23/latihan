<section class="footer-area pt-60 pb-60" align="right">
				<div class="container" align="center">
					
					<footer align="center">
							<div class="footer-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-dribbble"></i></a>
								<a href="#"><i class="fa fa-behance"></i></a>
							</div>
							<div class="footer-content">
								<div class="text-center">
									Copyright © 2018 All rights reserved   |   This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com">Colorlib</a>
							</div>
						</div>
					</footer>
				</div>
			</section>

			<!-- End Footer Widget Area -->