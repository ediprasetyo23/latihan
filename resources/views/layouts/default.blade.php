<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="main-wrapper-first">
				<div class="hero-area relative">
					<header>
						@include('includes.header')
					</header>
					<!-- <nav id="sidebar">
						@include('includes.sidebar')
					</nav> -->

					<div class="banner-area relative">
							<div class="overlay hero-overlay-bg"></div>
							<div class="container">
								<div class="row align-items-center justify-content-center">
									<div class="col-lg-10">
										<br>
										@yield('content')
										<br>
										<br>
										<br>
										<br>
																					
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<div class="main-wrapper">
				@include('includes.footer')
			</div>


		</div>
	</div>
</body>
</html>