@extends('layouts/default')

@section('content')

<div class="banner-content text-center">
<h1 class="text-uppercase text-white"><span>Profil</span></h1>
<br>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body" id="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<table class="table table-striped" style="border:3px solid black;color:black;background:white">

	<tr style="border:2px solid black">
		<th>Nama Lengkap</th>
		<th>Email</th>
		<th>Group</th>
		<th></th>
	</tr>
	<tr>
		<td>{{ $user->full_name }}</td>
		<td>{{ $user->email }}</td>
		<td>{{ $user->group }}</td>
		<td>
			<button class="btn btn-warning" id="editbutton" onclick="confirm_dit(this.value)" value="{{ $user->id }}">Edit</button>
			
		</td>
	</tr>
	
</table>
<button type="button" class="btn btn-danger" id="addbutton"> Log out </button>
</div>


@stop