<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Membuat Laporan PDF Dengan DOMPDF Laravel</h4>
	</center>
 	
	<table class='table table-striped' style='border:3px solid black;color:black;background:white'>
		<thead>
		<tr style='border:2px solid black'>";
			<th style='text-align: center'>Id</th>
			<th style='text-align: center'>Nama</th>
			<th style='text-align: center'>Email</th>
			<th style='text-align: center'>Nim</th>
		</tr>"
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($student as $p)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$p->name}}</td>
				<td>{{$p->email}}</td>
				<td>{{$p->nim}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>


	<!-- <table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Email</th>
				<th>Alamat</th>
				<th>Telepon</th>
				<th>Pekerjaan</th>
			</tr>
		</thead>
		<tbody>
		
		</tbody>
	</table> -->
 
</body>
</html>