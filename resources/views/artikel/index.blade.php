@extends('layouts/default')

@section('content')
<br>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body" id="modal-body">
        <img id="img_id" src="" style="width: 470px">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div style="float: right;padding-bottom: 10px">
	<button class="btn btn-primary" onclick="javascript:location.replace('/artikel/add');">Tambah Artikel</button>
</div>
<table class="table" style="color:black;background-color:rgba(255,255,255, 0.3);">	
	<tr>
        <th colspan="2" style="text-align: center;font-size: 30px;padding-left: 250px">Artikel</th>
    </tr>  
	<tr>@foreach($artikel as $art)
		<td style="padding-left:20px;border: none;"><img src="/{{ $art->filename }}" style="width: 200px;cursor: pointer;border: none;" onclick="viewImage(this.src)" value="{{ $art->filename }}"></td>@endforeach
	</tr>
	<tr>@foreach($artikel as $art)
		<td style="padding-left:20px;border: none">{{ $art->judul }}</td>@endforeach
	</tr>
	<tr>@foreach($artikel as $art)
		<td style="padding-left:20px;border: none;padding-bottom: 20px"><button class="btn btn-primary" onclick="javascript:location.replace('/artikel/read/{{ $art->id }}');">Read More</button>
		</td>@endforeach
	</tr>
</table>
 
<div style="float: center">
 {{ $artikel->links() }}
</div>
<script type="text/javascript">
	function viewImage(name){
		$('#img_id').attr('src',name);
		$('#myModal').modal();
	}
</script>

@stop