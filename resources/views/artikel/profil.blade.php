@extends('layouts/default')

@section('content')

<div class="banner-content text-center">
<h1 class="text-uppercase text-white"><span>Profil</span></h1>
<br>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body" id="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<table class="table table-striped" style="border:3px solid black;color:black;background:white">

	<tr style="border:2px solid black">
		<th>Nama Lengkap</th>
		<th>Email</th>
		<th>Group</th>
		<th></th>
	</tr>
	<tr>@foreach($artikel as $art)
		<td>{{ $art->full_name }}</td>
		<td>{{ $art->email }}</td>
		<td>{{ $art->group }}</td>
		<td>
			<button class="btn btn-warning" id="editbutton" onclick="confirm_dit(this.value)" value="{{ $art->id }}" onclick="javascript:location.replace('/artikel/editprofil');">Edit</button>
			
		</td>@endforeach
	</tr>
	
</table>
<button type="button" class="btn btn-danger" id="addbutton" onclick="javascript:location.replace('/');"> Log out </button>
</div>

<script type="text/javascript">
	function confirm_dit(id){
		/*console.log(id);*/
		$('#myModal').modal();
		$.ajax({
			url:"/artikel/editprofil/"+id,
			type:"get",
			dataType:"html",
			success:function(data){
				document.getElementById('myModalLabel').innerHTML="Tambah Data";

				document.getElementById('modal-body').innerHTML=data;
			}
		})
	}	
</script>
@stop