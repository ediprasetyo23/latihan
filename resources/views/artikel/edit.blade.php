@extends('layouts/default')

@section('content')
<br>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body" id="modal-body">
        <img id="img_id" src="" style="width: 470px">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<table class="table" style="color:black;background-color:rgba(255,255,255, 0.3);">
  <form action="/artikel/update" method="post" enctype="multipart/form-data">{{ csrf_field() }}
  <tr>
    <td style="padding-left:20px;border: none;">Image</td>
    <td style="padding-left:20px;border: none;"><input type="file" name="file"></td>
  </tr>
  @foreach($artikel as $art)
    <td style="padding-left:20px;border: none;"><input type="text" name="id" value="{{ $art->id }}" hidden></td>@endforeach
  <tr>@foreach($artikel as $art)
    <td style="padding-left:20px;border: none;">Judul</td>
    <td style="padding-left:20px;border: none;"><input type="text" name="judul" value="{{ $art->judul }}" style="width: 100%;"></td>@endforeach
  </tr>
  <tr>@foreach($artikel as $art)
    <td style="padding-left:20px;border: none;">Isi</td>
    <td style="padding-left:20px;border: none;"><textarea name="isi" value="{{ $art->isi }}" style="width: 100%;height: 250px;">{{ $art->isi }}</textarea></td>@endforeach
  </tr>
  <tr>
    <td style="padding-left:20px;border: none;" colspan="2">
      <input class="btn btn-warning" type="submit" name="submit" value="Save Artikel" style="float: right;border: none;"></td></form>  
  </tr>
  
      <button style="float: left;" class="btn btn-danger" onclick="javascript:location.replace('/artikel/index');">Cancel</button>
</table>


<script type="text/javascript">
</script>

@stop