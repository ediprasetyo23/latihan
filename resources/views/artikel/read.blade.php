@extends('layouts/default')

@section('content')
<br>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body" id="modal-body">
        <img id="img_id" src="" style="width: 470px">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<table class="table" style="color:black;background-color:rgba(255,255,255, 0.3);width: 100%;text-align:center">  
  <tr>@foreach($artikel as $art)
    <td style="padding-left:20px;border: none;float: right;">Penulis : {{ $art->penulis }}
    </td>@endforeach
  </tr>
  <tr>@foreach($artikel as $art)
    <td style="padding-left:20px;border: none;"><img src="/{{ $art->filename }}" style="width: 200px;cursor: pointer;border: none;" onclick="viewImage(this.src)" value="{{ $art->filename }}"></td>@endforeach
  </tr>
  <tr>@foreach($artikel as $art)
    <td style="padding-left:20px;border: none;font-weight: bold;">{{ $art->judul }}</td>@endforeach
  </tr>
  <tr>@foreach($artikel as $art)
    <td class="text" style="padding-left:20px;border: none;white-space: pre-wrap;
      word-wrap: break-word;">{{ $art->isi }}</td>@endforeach
  </tr>
  <tr>@foreach($artikel as $art)
    <td style="padding-left:20px;border: none;">
      <button style="float: left;" class="btn btn-primary" onclick="javascript:location.replace('/artikel/index');">Back</button>
      <button style="float: center;" class="btn btn-warning" onclick="javascript:location.replace('/artikel/edit/{{ $art->id }}');">Edit</button>
      <button style="float: right;" class="btn btn-danger" value="{{ $art->id }}" onclick="confirm_del(this.value)">Delete</button>
    </td>@endforeach
  </tr>
</table>

<script type="text/javascript">
	function viewImage(name){
		$('#img_id').attr('src',name);
		$('#myModal').modal();
	}
	function confirm_del(id){
		var conf=confirm("Delete confirm");
		if(conf){
			location.replace('/artikel/delete/'+id);
		}
	}
</script>

@stop