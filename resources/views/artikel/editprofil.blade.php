<!DOCTYPE html>
<html>

<head><h3> Edit Profil</h3></head>

<a href="/artikel/profil">Kembali</a>
<br/>
<br/>

<body>
@foreach($artikel as $art)
<form action="/artikel/updateprofil" method="post" >
{{ csrf_field() }}
<table border="1" cellpadding="5" cellspacing="1" align="center">
	<tr>
		<td>Email</td>
		<td><input type="text" name="email" required="required" value="{{ $art->email }}">
		<input type="hidden" name="id" value="{{ $art->id }}"></td>
		<input type="hidden" name="group" value="{{ $art->group }}"></td>
		<input type="hidden" name="create_at" value="{{ $art->create_at }}"></td>
	</tr>
	<tr>
		<td>Password</td>
		<td><input type="text" name="password" required="required" value="{{ $art->password }}">
	</tr>
	<tr>
		<td>Fullname</td>
		<td><input type="text" name="full_name" required="required" value="{{ $art->full_name }}">
	</tr>
	
	<tr>
		<td colspan="2"><input type="submit" value="Update Profil"></td>	
	</tr>

</table>
</form>

@endforeach

</body>

</html>