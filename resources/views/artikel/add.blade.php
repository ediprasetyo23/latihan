@extends('layouts/default')

@section('content')
<br>

<form action="/artikel/upload" method="post" enctype="multipart/form-data">{{ csrf_field() }}

<table class="table" style="color:black;background-color:rgba(255,255,255, 0.3);">
	<tr>
		<td style="padding-left:20px;border: none;">Image</td>
		<td style="padding-left:20px;border: none;"><input type="file" name="file"></td>
	</tr>
	<tr>
		<td style="padding-left:20px;border: none;">Judul</td>
		<td style="padding-left:20px;border: none;"><input type="text" name="judul" style="width: 100%;"></td>
	</tr>
	<tr>
		<td style="padding-left:20px;border: none;">Isi</td>
		<td style="padding-left:20px;border: none;"><textarea name="isi" style="width: 100%;height: 250px;"></textarea></td>
	</tr>
	<tr>
		<td style="padding-left:20px;border: none;" colspan="2">
			<input class="btn btn-warning" type="submit" name="submit" value="Save Artikel" style="float: right;border: none;"></td>	
	</tr>
</table>
</form>

<br>
<button style="float: left;" class="btn btn-primary" onclick="javascript:location.replace('/artikel/index');">Back</button>
<script type="text/javascript">
	function viewImage(name){
		$('#img_id').attr('src',name);
		$('#myModal').modal();
	}
</script>

@stop