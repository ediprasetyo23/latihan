<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/vue', function () {
    return view('vue');
});

Route::get('/home/index', 'HomeController@index')->name('index');
Route::post('/home/index', 'HomeController@index');
Route::get('/home/noauth', 'HomeController@noauth');

Route::get('/artikel/index', 'ArtikelController@index')->name('index');
Route::get('/artikel/read/{id}', 'ArtikelController@read')->name('read');
Route::get('/artikel/edit/{id}', 'ArtikelController@edit')->name('edit');
Route::get('/artikel/add', 'ArtikelController@add')->name('add');
Route::post('/artikel/upload', 'ArtikelController@upload');
Route::get('/artikel/delete/{id}', 'ArtikelController@delete');
Route::post('/artikel/update', 'ArtikelController@update');
Route::get('/artikel/profil', 'ArtikelController@profil')->name('profil');
Route::get('/artikel/editprofil/{id}', 'ArtikelController@editprofil')->name('editprofil');
Route::post('/artikel/updateprofil', 'ArtikelController@updateprofil');