<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/student/export_excel', 'StudentController@export_excel');

Route::get('/profile', 'ProfileController@index')->name('index');
Route::get('/profile/get/{id}', 'ProfileController@getprofile')->name('getprofile');
Route::post('/profile/create', 'ProfileController@create');
Route::post('/profile/update', 'ProfileController@update');
Route::get('/profile/delete/{id}', 'ProfileController@delete');

Route::get('/karyawan', 'KaryawanController@index')->name('index');


