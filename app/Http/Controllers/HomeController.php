<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    protected function index(Request $req){
        $user=DB::table('master_user')
        ->where('email','=',$req->email)
        ->where('password','=',$req->password)
        ->first();
        if(isset($user->id)){
            if($user->id){
                $req->session()->put('id',$user->id);
                $req->session()->put('email',$user->email);
                $req->session()->put('full_name',$user->full_name);
                $req->session()->put('group',$user->group);
                
                return view('/home/index',['user'=>$user]);
            }else{
                return view('/');
            }
        }else{
            return view('/home/noauth');
        }
    }
}
