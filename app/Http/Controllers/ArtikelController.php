<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtikelController extends Controller
{
    protected function index(){
        $artikel = DB::table('artikel')->simplePaginate(3);
        return view('/artikel/index',['artikel' => $artikel]);
    }
    public function read($id){
        $artikel= DB::table('artikel')->where('id', $id)->get();
        return view('artikel/read',['artikel' => $artikel]);
    }
    public function edit($id){
        $artikel= DB::table('artikel')->where('id', $id)->get();
        return view('artikel/edit',['artikel' => $artikel]);
    }
    protected function add(){
        return view('/artikel/add');
    }
    public function upload(Request $request){
        $file=$request->file('file');
    
    //Display File Name
    echo 'File Name:'.$file->getClientOriginalName();
    echo '<br>';
    //Display File Extension
    echo 'File Extension:'.$file->getClientOriginalExtension();
    echo '<br>';
    //Display File Real Path
    echo 'File Real Path:'.$file->getRealPath();
    echo '<br>';
    //Display File Size
    echo 'File Size:'.$file->getSize();
    echo '<br>';
    //Display File Mime Type
    echo 'File Mime Type:'.$file->getMimeType();
    //Move Uploaded File
    $destinationPath='uploads';
    $cek=$file->move($destinationPath,$file->getClientOriginalName());
    
    $filename=$destinationPath."/".$file->getClientOriginalName();
    if($cek){
        DB::table('artikel')->insert([
            'judul' => $request->judul,
            'filename' => $filename,
            'isi' => $request->isi,
            'penulis' => $request->session()->get('full_name'),
            'create_at' => date('Y-m-d H:i:s')
        ]);
        return redirect('/artikel/index');
    }
    }
    public function update(Request $request){
        $file=$request->file('file');
    
    //Display File Name
    echo 'File Name:'.$file->getClientOriginalName();
    echo '<br>';
    //Display File Extension
    echo 'File Extension:'.$file->getClientOriginalExtension();
    echo '<br>';
    //Display File Real Path
    echo 'File Real Path:'.$file->getRealPath();
    echo '<br>';
    //Display File Size
    echo 'File Size:'.$file->getSize();
    echo '<br>';
    //Display File Mime Type
    echo 'File Mime Type:'.$file->getMimeType();
    //Move Uploaded File
    $destinationPath='uploads';
    $cek=$file->move($destinationPath,$file->getClientOriginalName());
    
    $filename=$destinationPath."/".$file->getClientOriginalName();
    if($cek){
        DB::table('artikel')->where('id',$request->id)->update([
            'judul' => $request->judul,
            'filename' => $filename,
            'isi' => $request->isi,
            'penulis' => $request->session()->get('full_name'),
            'create_at' => date('Y-m-d H:i:s')
        ]);
        return redirect('/artikel/index');
    }
    }
    public function delete($id){
        $artikel= DB::table('artikel')->where('id', $id)->delete();
        return redirect('/artikel/index');
    }
    public function profil(Request $request){
        $id = $request->session()->get('id');
        $artikel= DB::table('master_user')->where('id', $id)->get();
        return view('/artikel/profil',['artikel'=>$artikel]);
    }
    public function editprofil($id){
        $artikel= DB::table('master_user')->where('id', $id)->get();
        return view('artikel/editprofil',['artikel' => $artikel]);
    }
    public function updateprofil(Request $request){
        DB::table('master_user')->where('id',$request->id)->update([
            'email' => $request->email,
            'password' => $request->password,
            'full_name'=> $request->full_name,
            'group'=> $request->group,
            'create_at'=> $request->create_at
        ]);
        return redirect('/artikel/profil');
    }
}
