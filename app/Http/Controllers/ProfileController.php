<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facader\DB;
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
    	$profile=Profile::all();
    	return $profile;
    }
    public function getprofile(Request $request){
    	$profile=Profile::find($request->id);
    	return $profile;
    }
    public function create(Request $request){
    	$profile=new Profile();
    	$profile->fullname=$request->fullname;
    	$profile->gender=$request->gender;
    	$profile->pdob=$request->pdob;
    	$profile->address=$request->address;
    	$profile->education=$request->education;
    	if($profile->save()){
    		return 'Berhasil';
    	}
    }
    public function update(Request $request){
        $profile=Profile::find($request->id);
        $profile->fullname=$request->fullname;
        $profile->gender=$request->gender;
        $profile->pdob=$request->pdob;
        $profile->address=$request->address;
        $profile->education=$request->education;
        if($profile->save()){
            return 'Berhasil';
        }
    }
    public function delete(Request $request){
        $profile= Profile::find($request->id);
        $profile->delete();
        return 'Berhasil';
    }
}
