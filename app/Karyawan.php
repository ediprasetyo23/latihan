<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table='t2_employee';
    protected $fillable=['name','gender'];
}
